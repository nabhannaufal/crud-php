-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2021 at 04:53 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_transaksi` varchar(50) NOT NULL,
  `tanggal` varchar(20) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `kode_barang` varchar(100) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `perusahaan` varchar(50) NOT NULL,
  `jumlah` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_transaksi`, `tanggal`, `lokasi`, `kode_barang`, `nama_barang`, `perusahaan`, `jumlah`) VALUES
('WG-202103751842', '14/09/2021', 'Gorontalo', '123124126', 'Roti', 'BK', '11'),
('WG-202113924670', '17/09/2021', 'Lampung', '129312312351', 'Mie Instant', 'GJK', '8'),
('WG-202134852067', '08/09/2021', 'Sulawesi Selatan', '123124123', 'Minyak', 'KFC', '2'),
('WG-202170128346', '16/09/2021', 'Yogyakarta', '123124124', 'Tepung', 'MCD', '20'),
('WG-202190578623', '15/09/2021', 'Sulawesi Selatan', '129312312356', 'Promag', 'GRB', '4');

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan`
--

CREATE TABLE `perusahaan` (
  `id_perusahaan` int(11) NOT NULL,
  `kode_perusahaan` varchar(100) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perusahaan`
--

INSERT INTO `perusahaan` (`id_perusahaan`, `kode_perusahaan`, `nama_perusahaan`) VALUES
(1, 'GJK', 'Gojek'),
(2, 'GRB', 'Grab'),
(5, 'MCD', 'McDonald'),
(6, 'KFC', 'Kentucky Fried Chicken'),
(7, 'BK', 'Burger King');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(10) NOT NULL,
  `id_transaksi` varchar(50) NOT NULL,
  `tanggal_masuk` varchar(20) NOT NULL,
  `tanggal_keluar` varchar(20) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `kode_barang` varchar(100) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `perusahaan` varchar(50) NOT NULL,
  `jumlah` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `id_transaksi`, `tanggal_masuk`, `tanggal_keluar`, `lokasi`, `kode_barang`, `nama_barang`, `perusahaan`, `jumlah`) VALUES
(1, 'WG-202013067948', '8/11/2020', '11/11/2020', 'NTB', '8888166995215', 'Ciki Lays', 'Unilever', '50'),
(2, 'WG-202013067948', '8/11/2020', '11/12/2020', 'NTB', '8888166995215', 'Ciki Lays', 'Unilever', '6'),
(3, 'WG-202013549728', '4/11/2020', '11/11/2020', 'Banten', '1923081008002', 'Iphone', 'Apple', '3'),
(4, 'WG-202074896520', '9/11/2020', '12/11/2020', 'Yogyakarta', '60201311121008264', 'Baterai', 'ZTE', '3'),
(5, 'WG-202027134650', '05/12/2020', '20/12/2020', 'Jakarta', '29312390203', 'Susu', 'Unilever', '17'),
(6, 'WG-202110974628', '15/01/2021', '16/01/2021', 'Lampung', '1923081008002', 'Sunlight', 'Unilever', '50'),
(7, 'WG-202081267543', '7/11/2020', '17/01/2021', 'Yogyakarta', '97897952889', 'Buku Framework Codeigniter', 'Unilever', '1'),
(8, 'WG-202132570869', '15/01/2021', '17/01/2021', 'Bali', '1923081008002', 'Test', 'Test', '10'),
(9, 'WG-202193850472', '15/01/2021', '18/01/2021', 'Bali', '1923081008002', 'Lumpur', 'Lapindo', '11'),
(10, 'WG-202081267543', '7/11/2020', '15/01/2021', 'Yogyakarta', '97897952889', 'Buku Framework Codeigniter', 'Unilever', '1'),
(11, 'WG-202027134650', '05/12/2020', '15/01/2021', 'Jakarta', '29312390203', 'Susu', 'Unilever', '3'),
(12, 'WG-202074896520', '9/11/2020', '15/01/2021', 'Yogyakarta', '60201311121008264', 'Baterai', 'ZTE', '3'),
(13, 'WG-202027134650', '05/12/2020', '16/01/2021', 'Jakarta', '29312390203', 'Susu', 'Unilever', '1'),
(14, 'WG-202027134650', '05/12/2020', '17/01/2021', 'Jakarta', '29312390203', 'Susu', 'Unilever', '1'),
(15, 'WG-202185472106', '18/01/2021', '19/01/2021', 'Riau', '8996001600146', 'Teh Pucuk', 'Unilever', '50'),
(16, 'WG-202171602934', '18/01/2021', '16/03/2021', 'Papua', '312212331222', 'Kopi Hitam', 'Unilever', '10'),
(0, 'WG-202190578623', '15/09/2021', '14/09/2021', 'Sulawesi Selatan', '129312312356', 'Promag', 'GJK', '4');

-- --------------------------------------------------------

--
-- Table structure for table `upload_gambar`
--

CREATE TABLE `upload_gambar` (
  `id` int(11) NOT NULL,
  `username_user` varchar(100) NOT NULL,
  `nama_file` varchar(220) NOT NULL,
  `ukuran_file` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `upload_gambar`
--

INSERT INTO `upload_gambar` (`id`, `username_user`, `nama_file`, `ukuran_file`) VALUES
(1, 'zahidin', 'nopic5.png', '6.33'),
(2, 'test', 'nopic4.png', '6.33'),
(3, 'coba', 'logo_unsada1.jpg', '16.69'),
(4, 'admin', 'nopic2.png', '6.33');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(12) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT 0,
  `last_login` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `role`, `last_login`) VALUES
(20, 'admin', 'admin@gmail.com', '$2y$10$3HNkMOtwX8X88Xb3DIveYuhXScTnJ9m16/rPDF1/VTa/VTisxVZ4i', 1, '17-09-2021 16:39'),
(22, 'user', 'usser@mail.com', '$2y$10$UHhUnWIVpGB0MqKWS14Iaud5BsYRlNJ1VTeSbMdpLztjI.ymplwJu', 0, '17-09-2021 15:57'),
(23, 'nabhan', 'nabhan@mal.com', '$2y$10$CYRAndivQnXKH70fjN4/fejR6EzxxL7rA9fnaB7oHI4ExuKfp2rmG', 1, '17-09-2021 16:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`id_perusahaan`);

--
-- Indexes for table `upload_gambar`
--
ALTER TABLE `upload_gambar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `id_perusahaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `upload_gambar`
--
ALTER TABLE `upload_gambar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
